Kumpulan Berbagai HOWTO
=======================

Mungkin ada kekasih yang membutuhkan. Jika ada sesuatu, bisa disampaikan di bagian `issues`.

Daftar Isi:
-----------
* [Pemrosesan gambar dan PDF](imageNpdf.md)
* [Pembuatan Server Bridge](serverBridge.md)
* [Membuka File aneka ragam di Linux](variousFiles.md)