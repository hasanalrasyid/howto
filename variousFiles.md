Opening Various Files in Linux
==============================

SWF (Adobe Shockwave Files)
---------------------------

Istri dapat aplikasi lawas tentang simulasi pergerakan matahari (saya ga paham).
Tersimpan sebagai file .swf yang adalah aplikasi Flash (ini saya paham).
Cara buka di firefox, kalau tak bisa langsung di buka.

Tambahkan di `~/.mime.types` :

```
application/x-shockwave-flash       swf
```

Buka `about.config` di Firefox, ubah parameternya menjadi:

```
plugins.http_https_only = false
```

Sekarang file .swf bisa dibuka di Firefox.


