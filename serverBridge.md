Server Bridge
=============

Skenario
--------

Server T:
* Terhalang firewall, dalam network lokal intraT.
* Punya ssh-server yang melayani port 22.

machineT:
* Suatu mesin di dalam intraT. 

Server L:
* Server di DuniaLuas, kita punya akses penuh atasnya.
* Punya ssh-server yang melayani port 443 (https).

machineL:
* Dapat ssh ke serverL 

Tujuan
------

1. Akses dua arah:
   * intraT -> T -> L -> DuniaLuas
   * duniaLuas -> L -> T -> intraT

Ringkasan (tl;dr)
----------------

1. Buat `reverse forwarding` dari serverT di serverL menggunakan ssh.
2. Buat SOCKS proxy 

Petunjuk Terperinci
-----


```mermaid
sequenceDiagram
  autonumber
    participant machineT
    participant serverT
    
    participant serverL
    participant machineL
    
    
    serverT ->> serverL: ssh -p 443 serverL -R 2222:localhost:22 -D 8800
    
    Note over serverT: serverT:8800 SOCKS tunnel to DuniaLuas
    Note over serverL: serverL:2222 SSH port to serverT
    machineT ->> serverT: curl --proxy socks5://serverT:8800 "www.google.com"
    Note right of serverL: DuniaLuas

    serverL ->> serverT: ssh localhost -p 2222
    Note over serverL, serverT: serverL dapat langsung ssh ke serverT
    
    machineL ->> serverL: ssh serverL -L 4444:localhost:2222 <br> ssh localhost -p 4444 -D 8000
    Note over machineL, serverT: second layer tunnel
    Note over machineL: localhost:8000 SOCKS tunnel menuju intraT
    Note over machineL: localhost:4444 SSH port menuju serverT
    
    Note left of serverT: intraT

    machineL ->> serverT:  curl --proxy socks5://localhost:8000 "localsite.intraT.net"
    machineL ->> serverT: ssh localhost -p 4444
```
Langkah terperinci, tiap langkah berurutan dari atas ke bawah.

### Catatan

Step 1. Perintah ini bisa dirutinkan (daemonized/crond) sehingga kita bisa mengakses serverT melalui serverL kapan pun dari mana pun. Jika tidak, maka perlu ada orang di sekitar serverT yang melakukan perintah ini.
  `-D 8000` bisa dihilangkan jika hanya membutuhkan akses ke serverT dari Luar.

```
Tinjauan keamanan:
Pastikan firewall di semua server menolak akses ke port-port tersebut. 
Skenario terburuk, seseorang di DuniaLuas dapat mengakses serverT melalui perintah 
ssh -p 2222 serverL_IP
```


Step 2. Contoh penggunaan SOCKS proxy memakai curl. Parameter proxy bervariasi untuk setiap browser/client. Skenario yang lebih aman ada di Step 4.

Step 3. Sudah jelas

Step 4. Pembuatan second layer tunnel dianjurkan agar integritas serverL tetap terjaga. Pada prinsipnya, semua port yang terbuka harus hanya bisa diakses dari localhost. Cara ini (dengan penyesuaian seperlunya), lebih dianjurkan daripada langkah (1) yang membuka port 8000 ke publik (meski sebatas jaringan intraT).

Step 5. Sudah jelas

Step 6. Sudah jelas

Demi kesederhanaan, di sini tampil perintah minimal. Kita bisa tambahkan parameter lain yang memudahkan, misalnya `-N` untuk mencegah masuk shell. 

